export const DarkTheme = {
    bgColor : '#343a40' ,
    color : '#FFFFFF' , 
    borderButton : '1px solid #FFF' , 
    borderRadiusButton : 'none' ,
    hoverTextColor : '#343a40' , 
    hoverBgColor : '#FFFFFF' , 
    borderColor : '#343a40'
}